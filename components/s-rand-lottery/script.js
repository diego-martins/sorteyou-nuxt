import _ from 'lodash'

export default {
    name: 'SRandLottery',
    data: function () {
        return {
            qtdDefault: 1,
            minValDefault: 1,
            maxValDefault: 100,
            minValRequired: 0,
            minQtdRequired: 1,
            maxValRequired: 10000,
            maxQtdRequired: 100,
            minMaxVal: "0/0",
            minVal: 0,
            maxVal: 0,
            qtd: 0,
            minMaxVal2: "0/0",
            minVal2: 0,
            maxVal2: 0,
            qtd2: 0,
            isLoading: false,
            errors: [],
            results: [],
            results2: [],
            selectedType: 0,
            selectedText: '',
            optionsTypeUS: [
                { text: 'PowerBall', value: 3, min: 1, max: 69, qtd: [5], min2: 1, max2: 26, qtd2: [1] },
                { text: 'MegaMillions', value: 4, min: 1, max: 75, qtd: [5], min2: 1, max2: 15, qtd2: [1] }
            ],
            optionsTypeBR: [
                { text: 'Mega Sena', value: 0, min: 1, max: 60, qtd: [6,7,8,9,10,11,12,13,14,15] },
                { text: 'Loto Fácil', value: 1, min: 1, max: 25, qtd: [15,16,17,18] },
                { text: 'Quina', value: 2, min: 1, max: 80, qtd: [5,6,7,8,9,10,11,12,13,14,15] }
            ],              
            optionsQtd: [],
            optionsQtd2: []
        }
    },
    created: function () {
        this.selected = 0
    },
    computed: {
        optionsType: function () {
            if(this.$store.state.locale == 'en'){
                setTimeout(()=>{this.selected = 0}, 200)
                return this.optionsTypeUS
            }else{
                setTimeout(()=>{this.selected = 0}, 200)
                return this.optionsTypeBR                
            }
        },
        selected: {
            get: function () {
                return this.selectedType
            },
            set: function (value) {
                
                this.results = []
                this.results2 = []
                this.errors = []

                this.selectedType = value
                const option = this.optionsType[value]
                this.minVal = option.min
                this.maxVal = option.max
                this.minMaxVal = this.minVal + "/" + this.maxVal
                this.optionsQtd = option.qtd
                this.qtd = option.qtd[0]

                this.minVal2 = option.min2||0
                this.maxVal2 = option.max2||0
                this.minMaxVal2 = this.minVal2 + "/" + this.maxVal2
                this.optionsQtd2 = option.qtd2||[0]
                this.qtd2 = this.optionsQtd2[0]
            }
        },        
        resultBalls: function () {
            let _return = []
            this.results.forEach(item => {
                _return.push({
                    value: (item<10) ? '0' + item : item,
                    type: 'ball-1'
                })
            })
            this.results2.forEach(item => {
                _return.push({
                    value: (item<10) ? '0' + item : item,
                    type: 'ball-2'
                })
            })
            return _.orderBy(_return, ['type', 'value'])
        },
        resultText: function () {
            let _return = ''
            let count = this.results.length
            for (let i = 0; i < count; i++) {
                if (_return) {
                    _return += ' - '
                }
                _return += (this.results[i]<10) ? '0' + this.results[i] : this.results[i]
            }
            return _return
        },
        resultText2: function () {
            let _return = ''            
            let count = this.results2.length            
            for (let i = 0; i < count; i++) {
                if (_return) {
                    _return += ' - '
                }
                _return += (this.results2[i]<10) ? '0' + this.results2[i] : this.results2[i]
            }
            return _return
        },
        resultTextCopy: function () {
            if(this.resultText2){
                return this.resultText + " / " + this.resultText2
            }
            return this.resultText
        }
    },
    methods: {
        getSelectedText: function () {
            const selectedObj = this.optionsType[this.selectedType]
            return (selectedObj) ? selectedObj.text : ''
        },
        isValid: function () {
            this.errors = []
            this.results = []
            this.results2 = []

            this.minVal = parseInt(this.minVal||'0')
            this.maxVal = parseInt(this.maxVal||'0')
            this.qtd = parseInt(this.qtd||'0')

            this.minVal2 = parseInt(this.minVal2||'0')
            this.maxVal2 = parseInt(this.maxVal2||'0')
            this.qtd2 = parseInt(this.qtd2||'0')

            if(this.minVal == this.maxVal){
                this.errors.push(this.$t('error_vals_equals'))
            }

            if (this.minVal < this.minValRequired || this.minVal > this.maxValRequired) {
                this.errors.push(this.$t('error_val_initial_between', {min: this.minValRequired, max: this.maxValRequired}))
            }

            if (this.maxVal < this.minValRequired || this.maxVal > this.maxValRequired) {
                this.errors.push(this.$t('error_val_final_between', {min: this.minValRequired, max: this.maxValRequired}))
            }

            if(this.errors.length == 0){
                if(this.minVal > this.maxVal){
                    this.errors.push(this.$t('error_val_initial_greater'))
                }
                else if(this.qtd > this.maxVal){
                    this.errors.push(this.$t('error_qtd_greater_final'))
                }
                else if (this.qtd < this.minQtdRequired || this.qtd > this.maxQtdRequired) {
                    this.errors.push(this.$t('error_qtd'))
                }    
            }

            return (this.errors.length == 0)
        },
        btnPerform: function () {
            if(this.isLoading) return
            this.$store.commit('setLoading', true)
            this.isLoading = true
            setTimeout(()=>{
                if (this.isValid()) {
                    while (this.results.length < this.qtd) {
                        const value = this.getRandom(this.minVal, this.maxVal)
                        let has = false
                        for (let i = 0; i < this.results.length; i++) {
                            if (this.results[i] == value) {
                                has = true
                                break
                            }
                        }
                        if (!has) {
                            this.results.push(value)
                        }
                    }
                    
                    while (this.results2.length < this.qtd2) {
                        const value2 = this.getRandom(this.minVal2, this.maxVal2)
                        let has2 = false
                        for (let i = 0; i < this.results2.length; i++) {
                            if (this.results2[i] == value2) {
                                has2 = true
                                break
                            }
                        }
                        if (!has2) {
                            this.results2.push(value2)
                        }
                    }
                }                
                this.$store.commit('setLoading', false)
                this.isLoading = false    
            }, 500)
            this.$ga.event('Random Button', 'click', `Lottery: ${this.getSelectedText()}`)
        },
        getRandom: function (min, max) {
            min = parseInt(min)
            max = parseInt(max)
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
}