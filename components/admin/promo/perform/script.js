import alert from '@/components/alert.vue'
import confirm from '@/components/confirm.vue'
import dialogText from '@/components/dialog-text.vue'

export default {
    props: ['promoData'],
    name: 'Perform',
    components: { alert, confirm, dialogText },
    data: function () {
        return {
            id: '',
            name: '',
            startAt: '',
            finishAt: '',
            performAt: '',
            imageUrl: '',
            videoId: '',
            fakeImage: false,
            minUsers: 0,
            maxUsers: 0,
            maxWinners: 0,
            totalUsers: 0,
            totalWinners: 0,
            closedAt: null,
            closedType: null,
            closedText: '',
            publishType: null,
            closeTypes: [],
            _visible: false,
            _visibleBadWinners: false,
            _checked: false,
            winners: [],
            badWinners: [],
            form: {
                closedType: null,
                closedText: '',
                deleteWinnerText: '',
                notifyWinnerText: '',
            },
            selectedWinner: null,
            loadingPerform: false
        }
    },
    created: function () {
        this.id = this.$route.params.id
        this.form.notifyWinnerText = this.$t('text_message_winner_default')
        this.loadDataFromData(this.promoData)
        this.loadWinners()
    },
    computed: {
        checkedAtLeastOne: function () {
            return this.winners.filter(item => item.checked).length > 0
        },
        visible: {
            get: function () {
                let countVisible = 0
                let countInvisible = 0
                this.winners.forEach(item => {
                    if (item.visible) {
                        countVisible++
                    } else {
                        countInvisible++
                    }
                })
                if (countVisible == this.winners.length) {
                    this._visible = true
                }
                if (countInvisible == this.winners.length) {
                    this._visible = false
                }
                return this._visible
            },
            set: function (value) {
                this._visible = value
                this.winners.forEach(item => {
                    item.visible = value
                })
            }
        },
        visibleBadWinners: {
            get: function () {
                let countVisible = 0
                let countInvisible = 0
                this.badWinners.forEach(item => {
                    if (item.visible) {
                        countVisible++
                    } else {
                        countInvisible++
                    }
                })
                if (countVisible == this.badWinners.length) {
                    this._visibleBadWinners = true
                }
                if (countInvisible == this.badWinners.length) {
                    this._visibleBadWinners = false
                }
                return this._visibleBadWinners
            },
            set: function (value) {
                this._visibleBadWinners = value
                this.badWinners.forEach(item => {
                    item.visible = value
                })
            }
        },
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        },
        selectedWinnerName: function () {
            if (this.selectedWinner && this.selectedWinner.name) {
                return this.selectedWinner.name
            }
            return ''
        },
        imageUrlPerform: function () {
            if (this.videoId) {
                if (this.videoId) {
                    return `https://img.youtube.com/vi/${this.videoId}/sddefault.jpg`
                }
                return '/img/no-video.jpg'
            }
            if (this.imageUrl) {
                return this.imageUrl
            }
            return '/img/no-image.jpg'
        },
        btnPerformText: function () {
            if (this.closedType === 'success') {
                return this.$t('btn_promo_closed')
            } else if (this.closedType === 'cancel') {
                return this.$t('btn_promo_canceled')
            } else {
                return this.$t('btn_perform')
            }
        }
    },
    methods: {
        partMail: function (email) {
            if (!email) {
                return ''
            }
            let parts = email.split('@')
            let first = parts[0]
            if (!first) {
                return ''
            } else if (first.length === 1) {
                return '***@' + parts[1]
            }
            first = first.substr(0, first.length / 2)
            return first + '***@' + parts[1]
        },
        loadWinners: function () {
            this.$axios.$get(`/admin/promos/${this.id}/winners`)
            .then(res => {
                this.winners = []
                this.badWinners = []
                if (res.success) {
                    var data = res.data
                    data.forEach((item, index) => {
                        if (item.deletedAt) {
                            this.badWinners.push({
                                id: item.id,
                                name: item.name,
                                email: item.email,
                                createdAt: item.createdAt,
                                wonAt: item.wonAt,
                                wonPosition: item.wonPosition,
                                deletedAt: item.deletedAt,
                                deletedText: item.deletedText,
                                visible: false,
                                checked: false
                            })
                        } else {
                            this.winners.push({
                                id: item.id,
                                name: item.name,
                                email: item.email,
                                createdAt: item.createdAt,
                                wonAt: item.wonAt,
                                wonPosition: item.wonPosition,
                                visible: false,
                                checked: false
                            })
                        }
                    })
                }
            })
            .catch(err => {
                console.error(err)
            })
        },
        loadDataFromData: function (data) {
            if(!data) return
            this.name = data.name
            this.desc = data.desc
            this.terms = data.terms
            this.startAt = data.startAt
            this.finishAt = data.finishAt
            this.performAt = data.performAt
            this.imageUrl = data.imageUrl
            this.fakeImage = data.fakeImage
            this.videoId = data.videoId
            this.minUsers = data.minUsers
            this.maxUsers = data.maxUsers
            this.maxWinners = data.maxWinners
            this.totalUsers = data.totalUsers
            this.totalWinners = data.totalWinners
            this.closedAt = data.closedAt
            this.closedType = data.closedType || null
            this.closedText = data.closedText || ''
            this.publishType = data.publishType
            this.closeTypes = [
                { value: null, text: this.$t('option_close_cause') },
                { value: 'success', text: this.$t('option_close_success'), disabled: (this.maxWinners !== this.totalWinners) },
                { value: 'cancel', text: this.$t('option_close_cancel') }
            ]
            this.form.closedType = this.closedType
            this.form.closedText = this.closedText
        },
        loadData: function () {
            this.loading = true
            this.$axios.$get(`/admin/promos/${this.id}`)
            .then(res => {
                this.loading = false
                if (res.success) {
                    var data = res.data
                    this.name = data.name
                    this.desc = data.desc
                    this.terms = data.terms
                    this.startAt = data.startAt
                    this.finishAt = data.finishAt
                    this.performAt = data.performAt
                    this.imageUrl = data.imageUrl
                    this.fakeImage = data.fakeImage
                    this.videoId = data.videoId
                    this.minUsers = data.minUsers
                    this.maxUsers = data.maxUsers
                    this.maxWinners = data.maxWinners
                    this.totalUsers = data.totalUsers
                    this.totalWinners = data.totalWinners
                    this.closedAt = data.closedAt
                    this.closedType = data.closedType || null
                    this.closedText = data.closedText || ''
                    this.publishType = data.publishType
                    this.closeTypes = [
                        { value: null, text: this.$t('option_close_cause') },
                        { value: 'success', text: this.$t('option_close_success'), disabled: (this.maxWinners !== this.totalWinners) },
                        { value: 'cancel', text: this.$t('option_close_cancel') }
                    ]
                    this.form.closedType = this.closedType
                    this.form.closedText = this.closedText
                } else {
                    this.$router.push('/ops#404')
                }
            })
            .catch(err => {
                this.loading = false
                console.error(err)
                if (err.response.status === 401) {
                    this.$router.push('/ops#401')
                } else {
                    this.$router.push('/ops#500')
                }
            })
        },
        btnClosePromo: function () {
            if (this.closedType === 'success') {
                this.$refs.alert.showError('Ops', this.$t('error_promo_already_closed'))
                return
            }
            if (this.closedType === 'cancel') {
                this.$refs.alert.showError('Ops', this.$t('error_promo_canceled'))
                return
            }
            this.$refs.confirm.show(
                this.$t('label_confirm'),
                this.$t('text_confirm_close_promo'),
                { color: 'red' }
            ).then(confirmResult => {
                if (confirmResult) {
                    this.$refs.dialogText.show(
                        this.$t('label_close_promo'),
                        this.$t('placeholder_close_promo'),
                        200,
                        true,
                        { color: 'green' }
                    ).then(resultText => {
                        if (!!resultText) {
                            this._closePromo('success', resultText)
                        }
                    })
                }
            })
        },
        btnCancelPromo: function () {
            if (this.closedType === 'success') {
                this.$refs.alert.showError('Ops', this.$t('error_promo_closed'))
                return
            }
            if (this.closedType === 'cancel') {
                this.$refs.alert.showError('Ops', this.$t('error_promo_already_canceled'))
                return
            }
            this.$refs.confirm.show(
                this.$t('label_confirm'),
                this.$t('text_confirm_cancel_promo'),
                { color: 'red' }
            ).then(confirmResult => {
                if (confirmResult) {
                    this.$refs.dialogText.show(
                        this.$t('label_cancel_promo'),
                        this.$t('placeholder_cancel_promo'),
                        200,
                        true,
                        { color: 'red' }
                    ).then(resultText => {
                        if (!!resultText) {
                            this._closePromo('cancel', resultText)
                        }
                    })
                }
            })
        },
        btnExportUsers: function () {
            this.loading = true
            this.$axios.$get(`/admin/promos/${this.id}/exportUsers`)
            .then(res => {
                this.loading = false
                if (res.success) {
                    this.$refs.alert.showSuccess('', res.message)
                } else {
                    this.$refs.alert.showError('Ops', res.message)
                }
            })
            .catch(err => {
                this.loading = false
                console.error(err)
                if (err.response && err.response.data) {
                    this.$refs.alert.showError('Ops', err.response.data)
                }
            })
        },
        _closePromo: function (closeType, closeText) {
            this.loading = true
            let formData = {
                type: closeType,
                text: closeText
            }
            this.$axios.$put(`/admin/promos/${this.id}/close`, formData)
            .then(res => {
                this.loading = false
                if (res.success) {
                    this.loadData()
                    if (closeType === 'success') {
                        this.$refs.alert.showSuccess('Ops', this.$t('success_close_promo'))
                    } else {
                        this.$refs.alert.showSuccess('Ops', this.$t('success_cancel_promo'))
                    }
                } else {
                    this.$refs.alert.showError('Ops', res.message)
                }
            })
            .catch(err => {
                this.loading = false
                console.error(err)
                if (err.response && err.response.data) {
                    this.$refs.alert.showError('Ops', err.response.data, { color: 'red' })
                }
            })
        },
        btnPerform: function () {
            this.loading = true
            this.loadingPerform = true
            this.$axios.$put(`/admin/promos/${this.id}/perform`)
            .then(res => {
                this.loading = false
                this.loadingPerform = false
                if (!res.success) {
                    this.$refs.alert.showError('Ops', res.message)
                } else {
                    this.loadData()
                    this.loadWinners()
                    if (res.message) {
                        this.$refs.alert.showSuccess('Ops', res.message)
                    }
                }
            })
            .catch(err => {
                this.loading = false
                this.loadingPerform = false
                console.error(err)
                if (err.response && err.response.data) {
                    this.$refs.alert.showError('Ops', err.response.data)
                }
            })
        },
        btnNofityWinner: function (id) {
            const winner = this.winners.find(item => item.id === id)
        },
        btnDeleteWinner: function (id) {

            const winner = this.winners.find(item => item.id === id)

            if (winner) {
                //confirm
                this.$refs.confirm.show(
                    this.$t('label_delete'),
                    this.$t('text_confirm_delete_winner', { userName: winner.name }),
                    { color: 'red', html: true }
                ).then(confirmResult => {

                    if (confirmResult) {
                        //get text about deletion
                        this.$refs.dialogText.show(
                            this.$t('label_delete'),
                            this.$t('label_delete_winner_text'),
                            200,
                            true,
                            { color: 'red' }
                        ).then(textResult => {
                            if (textResult) {
                                this._deleteWinner(id, textResult)
                            }
                        })
                    }
                })
            }
        },
        _deleteWinner: function (id, text) {
            this.loading = true
            let formData = {
                winner: id,
                text
            }

            this.$axios.$post(`/admin/promos/${this.id}/deleteWinner`, formData)
            .then(res => {
                this.loading = false
                if (!res.success) {
                    this.$refs.alert.showError('Ops', res.message)
                } else {
                    this.loadData()
                    this.loadWinners()
                    if (res.message) {
                        this.$refs.alert.showSuccess('Ops', res.message)
                    }
                }
            })
            .catch(err => {
                this.loading = false
                console.error(err)
                if (err.response && err.response.data) {
                    this.$refs.alert.showError('Ops', err.response.data)
                }
            })
        },
        btnShowDeletedMessage: function (id) {
            const badWinner = this.badWinners.find(item => item.id === id)
            if(badWinner) {
                this.$refs.alert.showError(
                    this.$t('label_deleted_winner_text'),
                    badWinner.deletedText
                )
            }
        },
        btnNotifyWinner: function (id) {
            const winner = this.winners.find(item => item.id === id)
            if (winner) {
                this.$refs.dialogText.show(
                    this.$t('label_message'),
                    this.$t('text_message_winner', {userName: winner.name }),
                    500,
                    true,
                    { btnContinueText: this.$t('btn_send') }
                ).then(textResult => {
                    if (textResult) {
                        this._notifyWinner(id, textResult)
                    }
                })
            }
        },
        _notifyWinner: function (id, text) {

            this.loading = true
                let formData = {
                    winner: id,
                    message: text
                }
                this.$axios.$post(`/admin/promos/${this.id}/notifyWinner`, formData)
                .then(res => {
                    this.loading = false
                    if (!res.success) {
                        this.$refs.alert.showError('Ops', res.message)
                    } else {
                        this.loadData()
                        this.loadWinners()
                        if (res.message) {
                            this.$refs.alert.showSuccess('Ops', res.message)
                        }
                    }
                })
                .catch(err => {
                    this.loading = false
                    console.error(err)
                    if (err.response && err.response.data) {
                        this.$refs.alert.showError('Ops', err.response.data)
                    }
                })

        }
    }
}