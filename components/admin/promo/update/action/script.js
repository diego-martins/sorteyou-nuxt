export default {
  name: 'Action',
  props: ['promo'],
  data: function() {
    return {
      form: {
        action: '',
        valid: false,
        rules: {
          action: [v => !!v || this.$t('error_field_required')]
        }
      },
      closedType: null,
      publishType: null,
      categories: [],
      errors: [],
      message: {
        success: true,
        data: []
      }
    }
  },
  computed: {
    enabled: function() {
      return !this.closedType && this.publishType !== 'prod'
    },
    loading: {
      get: function() {
        return this.$store.state.loading
      },
      set: function(value) {
        this.$store.commit('setLoading', value)
      }
    }
  },
  created: function() {
    const data = this.promo
    //this.form.action = data.action
    this.closedType = data.closedType
    this.publishType = data.publishType
    this.categories = [
      { k: 'facebook', v: 'Facebook' },
      { k: 'twitter', v: 'Twitter' },
      { k: 'instagram', v: 'Instagram' },
      { k: 'website', v: 'Website' }
    ]
  },
  methods: {
    showMessage: function(_success, _data) {
      this.message = {
        success: _success,
        data: Array.isArray(_data) ? _data : [_data]
      }
    },
    hideMessage: function() {
      this.message = {
        success: true,
        data: []
      }
    },
    dataValidation: function(form) {
      let errors = []
      if (!form.action) {
        errors.push(this.$t('comp.action.error_action_required'))
      }
      return errors.length ? errors : null
    },
    btnSubmit: function(e) {
      e.preventDefault()
      if (this.$refs.form.validate()) {
        this.submit()
      }
    },
    submit: function() {
      let form = this.form

      let errors = this.dataValidation(form)
      this.hideMessage()
      if (errors) {
        this.showMessage(false, errors)
        this.$scrollTo('.s-admin-promo-basic', { offset: -70 })
        return
      }

      let jsonData = {
        action: form.action
      }

      this.loading = true
      this.form.loading = true
      this.$axios
        .$put(`/admin/promos/${form.id}/action`, jsonData)
        .then(res => {
          this.form.loading = false
          this.loading = false
          if (res.success) {
            this.showMessage(true, res.message || this.$t('success_alter_data'))
          } else {
            this.showMessage(false, res.data.errors || [])
          }
          this.$scrollTo('.s-admin-promo-action', { offset: -70 })
        })
        .catch(err => {
          console.error(err)
          this.form.loading = false
          this.loading = false
          this.showMessage(false, this.$t('error_alter_data'))
          this.$scrollTo('.s-admin-promo-action', { offset: -70 })
        })
    }
  }
}
