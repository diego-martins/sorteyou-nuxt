import url from 'url'

export default {
    name: 'Media',
    props: ['promoData'],
    data: function () {
        return {
            form: {
                id: '',
                file: null,
                videoId: '',
                mediaType: 'image',
                imageUrl: null,
                loading: false,
                fakeImage: false
            },
            localFileName: '',
            localFileUrl: '',
            closedType: null,
            publishType: null,
            errors: [],
            message: {
                success: true,
                data: []
            },
            mediaOptions: []
        }
    },
    computed: {
        enabled: function () {
            return (!this.closedType && this.publishType !== 'prod')
        },
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        },
        videoUrl: {
            get: function () {
                return this.form.videoId
            },
            set: function (value) {
                const _value = this.getVideIdFromUrl(value)
                if (_value) {
                    this.form.videoId = _value
                } else {
                    this.form.videoId = value
                }
            }
        },
        imageUrlPerform: function () {
            if (this.form.mediaType === 'video') {
                if (this.form.videoId) {
                    return `https://img.youtube.com/vi/${this.form.videoId}/sddefault.jpg`
                }
                return '/img/no-video.jpg'
            }
            if (this.localFileUrl) {
                return this.localFileUrl
            } else if (this.form.imageUrl) {
                return this.form.imageUrl
            }
            return '/img/no-image.jpg'
        }
    },
    created: function () {
        const data = this.promoData
        this.form.id = data.id
        this.form.imageUrl = data.imageUrl
        this.form.videoId = data.videoId
        this.form.fakeImage = data.fakeImage
        this.videoId = data.videoId
        this.form.mediaType = data.videoId
            ? 'video'
            : 'image'
        this.closedType = data.closedType
        this.publishType = data.publishType
        
        this.mediaOptions = [
            { text: this.$t('label_image'), value: 'image' },
            { text: this.$t('label_video'), value: 'video' }
        ]
    },
    methods: {
        showMessage: function (_success, _data) {
            this.message = {
                success: _success,
                data: Array.isArray(_data)
                    ? _data
                    : [_data]
            }
        },
        hideMessage: function () {
            this.message = {
                success: true,
                data: []
            }
        },
        getVideIdFromUrl: function (value) {
            let videId = ''
            if (value) {
                const domains = ['youtu.be', 'youtube.com', 'www.youtube.com', 'm.youtube.com']
                const _url = url.parse(value)
                if (_url && _url.hostname && domains.includes(_url.hostname)) {
                    let _value = ''
                    if (_url.query) {
                        _value = _url.query.split('&')
                            .map(item => (item) ? item.split('=') : '')
                            .find(item => item && item.length === 2 && item[0] === 'v')
                        _value = (_value && _value.length === 2) ? _value[1] : ''
                    }
                    else if (_url.pathname) {
                        _value = _url.pathname.split('/')
                        _value = (_value && _value.length > 0) ? _value[_value.length - 1] : ''
                    }
                    if (_value) {
                        videId = _value
                    }
                }
            }
            return videId
        },
        pickFile() {
            this.$refs.file.click()
        },
        onFilePicked(e) {
            const files = e.target.files
            if (files[0] !== undefined) {
                this.localFileName = files[0].name
                if (this.localFileName.lastIndexOf('.') <= 0) {
                    return
                }
                const fr = new FileReader()
                fr.readAsDataURL(files[0])
                fr.addEventListener('load', () => {
                    this.localFileUrl = fr.result
                    this.form.file = files[0]
                })
                this.form.file = files[0]
            } else {
                this.file = null
                this.localFileName = ''
                this.localFileUrl = ''
            }
        },
        dataValidation: function (form) {
            let errors = []
            if (form.mediaType === 'video' && !form.videoId) {
                errors.push(this.$t('error_video_required'))
            }
            else if (form.mediaType === 'image' && !form.file) {
                errors.push(this.$t('error_image_required'))
            }
            return errors.length ? errors : null
        },
        btnSubmit: function (e) {
            e.preventDefault()
            this.submit()
        },
        submit: function () {
            let form = this.form
            let errors = this.dataValidation(form)
            this.hideMessage()
            if (errors) {
                this.showMessage(false, errors)
                this.$scrollTo('.s-admin-promo-media', { offset: -70 })
                return
            }

            let formData = new FormData()
            if (form.mediaType === 'video') {
                formData.append('videoId', encodeURIComponent(form.videoId))
            } else {
                formData.append('file', form.file)
                formData.append('fakeImage', form.fakeImage)
            }

            let config = {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }

            this.loading = true
            this.form.loading = true
            this.$axios.$put(`/admin/promos/${form.id}/media`, formData, config)
                .then(res => {
                    this.loading = false
                    this.form.loading = false
                    if (res.success) {
                        this.showMessage(true, res.message || this.$t('success_alter_data'))
                        this.form.file = null
                        this.localFileName = ''
                        this.form.imageUrl = res.data.imageUrl
                    } else {
                        this.showMessage(false, res.data.errors || [])
                    }
                    this.$scrollTo('.s-admin-promo-media', { offset: -70 })
                })
                .catch(err => {
                    console.error(err)
                    this.loading = false
                    this.form.loading = false
                    this.showMessage(false, this.$t('error_alter_data'))
                    this.$scrollTo('.s-admin-promo-media', { offset: -70 })
                })
        }
    }
}