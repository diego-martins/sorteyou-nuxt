import alert from '@/components/alert.vue'
import confirm from '@/components/confirm.vue'
import SPay from '@/components/s-pay'

export default {
    name: 'Payment',
    components: { alert, confirm, SPay },
    data: function () {
        return {
            currency: 'USD',
            form: {
                amount: 5,
            },
            message: {
                success: true,
                data: []
            }
        }
    },
    computed: {
        credits () {
            return this.$store.getters.admCredits
        },
        creditsLoading () {
            return this.$store.getters.admCreditsLoading
        }
    },
    methods: {
        payCallback: function (res) {
            if(res.success) {
                this.$store.dispatch('loadAdmCredits')
            }
        }
    }
}