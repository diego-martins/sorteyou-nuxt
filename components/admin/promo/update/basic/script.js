import countries from '@/assets/countries.json'

export default {
    name: 'Basic',
    props: ['promoData'],
    data: function () {
        return {
            form: {
                id: '',
                name: '',
                desc: '',
                terms: '',
                country: '',
                category: '',
                valid: false,
                rules: {
                    name: [
                        v => !!v || this.$t('error_field_required'),
                        v => (v && v.length <= 50) || this.$t('error_field_max_length', { length: 50 })
                    ],
                    desc: [
                        v => !!v || this.$t('error_field_required'),
                        v => (v && v.length <= 500) || this.$t('error_field_max_length', { length: 500 })
                    ],
                    terms: [
                        v => !!v || this.$t('error_field_required'),
                        v => (v && v.length <= 1000) || this.$t('error_field_max_length', { length: 1000 })
                    ],
                    country: [
                        v => !!v || this.$t('error_field_required')
                    ],
                    category: [
                        v => !!v || this.$t('error_field_required')
                    ],
                }
            },
            closedType: null,
            publishType: null,
            countries: [],
            categories: [],
            errors: [],
            message: {
                success: true,
                data: []
            }
        }
    },
    computed: {
        enabled: function () {
            return (!this.closedType && this.publishType !== 'prod')
        },
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        }
    },
    created: function () {
        const data = this.promoData
        this.form.id = data.id
        this.form.name = data.name
        this.form.desc = data.desc
        this.form.terms = data.terms
        this.form.category = data.category
        this.closedType = data.closedType
        this.publishType = data.publishType
        if (!data.country) {
            if (this.$store.state.locale == 'pt') {
                this.form.country = 'BR'
            } else {
                this.form.country = 'US'
            }
        } else {
            this.form.country = data.country
        }

        this.countries = countries
        this.categories = [
            { k: 'art', v: 'Arte e design' },
            { k: 'beauty', v: 'Beleza' },
            { k: 'decor', v: 'Casa e decoração' },
            { k: 'food', v: 'Comer e beber' },
            { k: 'shop', v: 'Compras' },
            { k: 'education', v: 'Educação' },
            { k: 'entreteniment', v: 'Entretenimento' },
            { k: 'sport', v: 'Esportes' },
            { k: 'lifestyle', v: 'Estilo de vida' },
            { k: 'event', v: 'Eventos' },
            { k: 'service', v: 'Serviços' },
            { k: 'health', v: 'Saúde e fitness' },
            { k: 'travel', v: 'Viagens' },
            { k: 'automotive', v: 'Automitivo' },
            { k: 'others', v: 'Outros' }
        ]
    },
    methods: {
        showMessage: function (_success, _data) {
            this.message = {
                success: _success,
                data: Array.isArray(_data)
                    ? _data
                    : [_data]
            }
        },
        hideMessage: function () {
            this.message = {
                success: true,
                data: []
            }
        },
        dataValidation: function (form) {
            let errors = [];

            if (!form.name) {
                errors.push(this.$t('error_name_required'))
            }

            if (!form.country) {
                errors.push(this.$t('error_country_required'))
            }

            if (!form.category) {
                errors.push(this.$t('error_category_required'))
            }

            if (!form.desc) {
                errors.push(this.$t('error_desc_required'))
            }

            if (!form.terms) {
                errors.push(this.$t('error_rules_required'))
            }

            return errors.length ? errors : null
        },
        btnSubmit: function (e) {
            e.preventDefault()
            if (this.$refs.form.validate()) {
                this.submit()
            }
        },
        submit: function () {
            let form = this.form

            let errors = this.dataValidation(form)
            this.hideMessage()
            if (errors) {
                this.showMessage(false, errors)
                this.$scrollTo('.s-admin-promo-basic', { offset: -70 })
                return
            }

            let jsonData = {
                name: form.name,
                desc: form.desc,
                terms: form.terms,
                country: form.country,
                category: form.category
            }

            this.loading = true
            this.form.loading = true
            this.$axios.$put(`/admin/promos/${form.id}/basic`, jsonData)
                .then(res => {
                    this.form.loading = false
                    this.loading = false
                    if (res.success) {
                        this.showMessage(true, res.message || this.$t('success_alter_data'))
                    } else {
                        this.showMessage(false, res.data.errors || [])
                    }
                    this.$scrollTo('.s-admin-promo-basic', { offset: -70 })
                })
                .catch(err => {
                    console.error(err)
                    this.form.loading = false
                    this.loading = false
                    this.showMessage(false, this.$t('error_alter_data'))
                    this.$scrollTo('.s-admin-promo-basic', { offset: -70 })
                })
        }
    }
}