import updateBasic from '@/components/admin/promo/update/basic'
import updateDates from '@/components/admin/promo/update/dates'
import updateMedia from '@/components/admin/promo/update/media'
import updateAdvanced from '@/components/admin/promo/update/advanced'
import updatePublish from '@/components/admin/promo/update/publish'
import payment from '@/components/admin/promo/update/payment'
import updateAction from '@/components/admin/promo/update/action'

export default {
  name: 'Update',
  props: ['promo'],
  components: {
    updateBasic,
    updateDates,
    updateMedia,
    updateAdvanced,
    updatePublish,
    payment,
    updateAction
  },
  computed: {
    totalCredits() {
      const total = this.$options.filters.currency(
        this.$store.getters.admCredits,
        'USD'
      )
      return `${this.$t('comp.credits.total').toUpperCase()}: ${total}`
    },
    publishName() {
      if (this.promo.publishType === 'prod') {
        return this.$t('label_prod')
      } else if (this.promo.publishType === 'test') {
        return this.$t('label_test')
      }
      return this.$t('label_dev')
    }
  }
}
