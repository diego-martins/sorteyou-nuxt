import moment from 'moment'

export default {
    name: 'Dates',
    props: ['promoData'],
    data: function () {
        return {
            form: {
                id: '',
                startDate: null,
                startTime: null,
                finishDate: null,
                finishTime: null,
                performDate: null,
                performTime: null,
                valid: false,
                rules: {
                    startDate: [
                        v => !!v || this.$t('error_field_required')
                    ],
                    startTime: [
                        v => !!v || this.$t('error_field_required')
                    ],
                    finishDate: [
                        v => !!v || this.$t('error_field_required')
                    ],
                    finishTime: [
                        v => !!v || this.$t('error_field_required')
                    ],
                    performDate: [
                        v => !!v || this.$t('error_field_required')
                    ],
                    performTime: [
                        v => !!v || this.$t('error_field_required')
                    ]
                }
            },
            modalStartDate: false,
            modalStartTime: false,
            modalFinishDate: false,
            modalFinishTime: false,
            modalPerformDate: false,
            modalPerformTime: false,
            closedType: null,
            publishType: null,
            errors: [],
            message: {
                success: true,
                data: []
            }
        }
    },
    created: function () {
        const data = this.promoData
        this.form.id = data.id
        this.form.startDate = moment(data.startAt).format('YYYY-MM-DD')
        this.form.startTime = moment(data.startAt).format('HH:mm')
        this.form.finishDate = moment(data.finishAt).format('YYYY-MM-DD')
        this.form.finishTime = moment(data.finishAt).format('HH:mm')
        this.form.performDate = moment(data.performAt).format('YYYY-MM-DD')
        this.form.performTime = moment(data.performAt).format('HH:mm')
        this.closedType = data.closedType
        this.publishType = data.publishType
    },
    computed: {
        startDateFormatted: function () {
            if (this.form.startDate && this.form.startDate.length == 10) {
                return moment(this.form.startDate).format(this.$t('date_format'))
            }
        },
        finishDateFormatted: function () {
            if (this.form.finishDate && this.form.finishDate.length == 10) {
                return moment(this.form.finishDate).format(this.$t('date_format'))
            }
        },
        performDateFormatted: function () {
            if (this.form.performDate && this.form.performDate.length == 10) {
                return moment(this.form.performDate).format(this.$t('date_format'))
            }
        },
        enabled: function () {
            return (!this.closedType && this.publishType !== 'prod')
        },
        locale: function () {
            return this.$store.state.locale || 'pt'
        },
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        }
    },
    methods: {
        showMessage: function (_success, _data) {
            this.message = {
                success: _success,
                data: Array.isArray(_data)
                    ? _data
                    : [_data]
            }
        },
        hideMessage: function () {
            this.message = {
                success: true,
                data: []
            }
        },
        dataValidation: function (form) {
            let errors = [];

            if (!form.startDate || !form.startTime) {
                errors.push(this.$t('error_initial_date_required'))
            } else if (!moment(`${form.startDate} ${form.startTime}`).isValid()) {
                errors.push(this.$t('error_initial_date_invalid'))
            }

            if (!form.finishDate || !form.finishTime) {
                errors.push(this.$t('error_final_date_required'))
            } else if (!moment(`${form.finishDate} ${form.finishTime}`).isValid()) {
                errors.push(this.$t('error_final_date_invalid'))
            }

            if (!form.performDate || !form.performTime) {
                errors.push(this.$t('error_perform_date_required'))
            } else if (!moment(`${form.performDate} ${form.performTime}`).isValid()) {
                errors.push(this.$t('error_perform_date_invalid'))
            }

            return errors.length ? errors : null
        },
        btnSubmit: function (e) {
            e.preventDefault()
            if (this.$refs.form.validate()) {
                this.submit()
            }
        },
        submit: function () {
            let form = this.form;

            let errors = this.dataValidation(form)
            this.hideMessage()
            if (errors) {
                this.showMessage(false, errors)
                this.$scrollTo('.s-admin-promo-dates', { offset: -70 })
                return
            }

            let jsonData = {
                startAt: moment(`${form.startDate} ${form.startTime}`).toJSON(),
                finishAt: moment(`${form.finishDate} ${form.finishTime}`).toJSON(),
                performAt: moment(`${form.performDate} ${form.performTime}`).toJSON()
            }

            this.loading = true
            this.form.loading = true
            this.$axios.$put(`/admin/promos/${form.id}/dates`, jsonData)
                .then(res => {
                    this.loading = false
                    this.form.loading = false
                    if (res.success) {
                        this.showMessage(true, res.message || this.$t('success_alter_data'))
                    } else {
                        this.showMessage(false, res.data.errors || [])
                    }
                    this.$scrollTo('.s-admin-promo-dates', { offset: -70 })
                })
                .catch(err => {
                    console.error(err)
                    this.loading = false
                    this.form.loading = false
                    this.showMessage(false, this.$t('error_alter_data'))
                    this.$scrollTo('.s-admin-promo-dates', { offset: -70 })
                })
        }
    }
}