import confirm from '@/components/confirm'

export default {
  name: 'Advanced',
  props: ['promo'],
  components: { confirm },
  data: function() {
    return {
      sliderMinUsers: {
        val: 1,
        min: 1
      },
      sliderMaxUsers: {
        val: 1,
        max: 2
      },
      sliderWinners: {
        val: 1,
        min: 1,
        max: 1
      },
      showPlans: false,
      plans: [],
      planItem: null,
      errors: [],
      message: {
        success: true,
        data: []
      }
    }
  },
  computed: {
    enabled: function() {
      return !this.promo.closedType && this.promo.publishType !== 'prod'
    },
    loading: {
      get: function() {
        return this.$store.state.loading
      },
      set: function(value) {
        this.$store.commit('setLoading', value)
      }
    },
    sliderMinUsersColor: function() {
      return !this.enabled ? 'grey' : 'primary'
    },
    sliderMaxUsersColor: function() {
      return this.promo.closedType ? 'grey' : 'primary'
    },
    sliderWinnersColor: function() {
      return !this.enabled ? 'grey' : 'primary'
    },
    sliderMinUsersMax: function() {
      if (!this.enabled) return this.sliderMinUsers.val
      return this.sliderMaxUsers.val - 1
    },
    sliderMaxUsersMin: function() {
      if (!this.enabled) return this.promo.maxUsers
      return this.sliderMinUsers.val + 1
    },
    sliderWinnersMax: function() {
      if (!this.enabled) return this.sliderWinners.val
      return this.sliderMinUsers.val < this.sliderWinners.max
        ? this.sliderMinUsers.val
        : this.sliderWinners.max
    },
    hasChange: function() {
      return (
        this.promo.minUsers !== this.sliderMinUsers.val ||
        this.promo.maxUsers !== this.sliderMaxUsers.val ||
        this.promo.plans.maxUsers !== this.sliderMaxUsers.max ||
        this.promo.maxWinners !== this.sliderWinners.val
      )
    }
  },
  created: function() {
    this.sliderMaxUsers.min = 1
    this.sliderMaxUsers.max = this.promo.plans.maxUsers
    this.sliderMaxUsers.val = this.promo.maxUsers

    this.sliderWinners.min = 1
    this.sliderWinners.max = this.promo.plans.maxWinners
    this.sliderWinners.val = this.promo.maxWinners

    this.loadPlans()
  },
  methods: {
    displayPrice(price) {
      if (parseFloat(price) === 0) {
        return this.$t('label_free')
      }
      return this.$options.filters.currency(price, 'USD', 0)
    },
    itemDisabled(item) {
      return (
        item.max < this.promo.plans.maxUsers ||
        parseFloat(item.price) > parseFloat(this.$store.getters.admCredits)
      )
    },
    loadPlans: function() {
      this.loading = true
      this.$axios
        .$get(`/admin/plans`)
        .then(res => {
          if (res.success && res.data) {
            this.plans = res.data
          }
          this.loading = false
        })
        .catch(err => {
          console.error(err)
          this.loading = false
        })
    },
    addMinUsers: function() {
      if (!this.enabled) return
      if (this.sliderMinUsers.val < this.sliderMaxUsers.val - 1) {
        this.sliderMinUsers.val++
      }
    },
    subMinUsers: function() {
      if (!this.enabled) return
      if (this.sliderMinUsers.val > this.sliderMinUsers.min) {
        this.sliderMinUsers.val--
      }
    },
    addMaxUsers: function() {
      if (this.sliderMaxUsers.val < this.sliderMaxUsers.max) {
        this.sliderMaxUsers.val++
      }
    },
    subMaxUsers: function() {
      if (this.sliderMaxUsers.val > this.sliderMinUsers.val + 1) {
        this.sliderMaxUsers.val--
      }
    },
    addMaxWinners: function() {
      if (!this.enabled) return
      if (this.sliderWinners.val < this.sliderWinners.max) {
        this.sliderWinners.val++
      }
    },
    subMaxWinners: function() {
      if (!this.enabled) return
      if (this.sliderWinners.val > this.sliderWinners.min) {
        this.sliderWinners.val--
      }
    },
    btnShowPlans: function() {
      if (this.plans.length) {
        const currentPlan = this.plans.find(
          item => item.max === this.sliderMaxUsers.max
        )
        this.planItem = currentPlan || this.plans[0]
        this.showPlans = true
      }
    },
    btnSavePlanItem: function() {
      this.sliderMaxUsers.max = this.planItem.max
      this.showPlans = false
    },
    formatNumber: function(val) {
      return this.$options.filters.number(val)
    },
    showMessage: function(_success, _data) {
      this.message = {
        success: _success,
        data: Array.isArray(_data) ? _data : [_data]
      }
    },
    hideMessage: function() {
      this.message = {
        success: true,
        data: []
      }
    },
    dataValidation: function(form) {
      let errors = []

      if (form.maxWinners > form.minUsers) {
        errors.push(this.$t('error_max_winners_min_users'))
      }

      return errors.length ? errors : null
    },
    btnSubmit: function(e) {
      e.preventDefault()
      if (this.sliderMaxUsers.max > this.promo.plans.maxUsers) {
        this.$refs.confirm
          .show(
            this.$t('label_confirm'),
            this.$t('comp.advanced.confirm_plan_change')
          )
          .then(result => {
            if (result) {
              this.submit()
            }
          })
      } else {
        this.submit()
      }
    },
    changePlan: function() {
      const self = this
      return new Promise((resolve, reject) => {
        self.loading = true
        const jsonData = {
          maxUsers: self.sliderMaxUsers.max
        }
        self.$axios
          .$put(`/admin/plans/promo/${self.promo.id}/maxUsers`, jsonData)
          .then(res => {
            resolved(res.data)
          })
          .catch(err => {
            console.error(err)
            self.loading = false
          })
      })
    },
    submit: function() {
      const self = this

      let form = {
        minUsers: this.sliderMinUsers.val,
        maxUsers: this.sliderMaxUsers.val,
        maxWinners: this.sliderWinners.val
      }

      let errors = this.dataValidation(form)
      this.hideMessage()
      if (errors) {
        this.showMessage(false, errors)
        this.$scrollTo('.s-admin-promo-advanced', { offset: -70 })
        return
      }

      const applyChange = function() {
        self.loading = true
        let jsonDataPromo = {
          minUsers: form.minUsers,
          maxUsers: form.maxUsers,
          maxWinners: form.maxWinners
        }

        self.$axios
          .$put(`/admin/promos/${self.promo.id}/advanced`, jsonDataPromo)
          .then(res => {
            self.loading = false
            if (res.success) {
              self.showMessage(
                true,
                res.message || self.$t('success_alter_data')
              )
            } else {
              self.showMessage(false, res.data.errors || [])
            }
            self.$scrollTo('.s-admin-promo-advanced', { offset: -70 })
            this.$store.dispatch('loadAdmPromo', { id: self.promo.id })
          })
          .catch(err => {
            console.error(err)
            self.loading = false
            self.showMessage(false, self.$t('error_alter_data'))
            self.$scrollTo('.s-admin-promo-advanced', { offset: -70 })
          })
      }

      if (this.sliderMaxUsers.max > this.promo.plans.maxUsers) {
        self.loading = true
        const jsonDataPlan = {
          maxUsers: self.sliderMaxUsers.max
        }
        self.$axios
          .$put(`/admin/plans/promo/${self.promo.id}/maxUsers`, jsonDataPlan)
          .then(res => {
            self.loading = false
            if (res.success) {
              self.$store.dispatch('loadAdmCredits')
              applyChange()
            } else {
              self.showMessage(false, res.message)
              self.$scrollTo('.s-admin-promo-advanced', { offset: -70 })
            }
          })
          .catch(err => {
            console.error(err)
            self.loading = false
            self.showMessage(false, self.$t('error_alter_data'))
            self.$scrollTo('.s-admin-promo-advanced', { offset: -70 })
          })
      } else {
        applyChange()
      }
    }
  }
}
