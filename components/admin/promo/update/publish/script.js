import alert from '@/components/alert.vue'
import confirm from '@/components/confirm.vue'

export default {
    name: 'Publish',
    props: ['promoData'],
    components: { alert, confirm },
    data: function () {
        return {
            id: '',
            name: '',
            closedType: null,
            publishType: null,
            publishTypeSelected: null,
            publishOptions: [],
            message: {
                success: true,
                data: []
            },
            loadingPublish: false
        }
    },
    created: function () {
        const data = this.promoData
        this.id = data.id
        this.name = data.name
        this.closedType = data.closedType
        this.publishType = data.publishType
        this.publishTypeSelected = data.publishType
        this.publishOptions = [
            {
                text: this.$t('btn_dev'),
                tooltip: this.$t('tooltip_publish_development'),
                value: null,
                disabled: (this.closedType != null || this.publishType === 'test' || this.publishType === 'prod')
            },
            {
                text: this.$t('btn_test'),
                tooltip: this.$t('tooltip_publish_test'),
                value: 'test',
                disabled: (this.closedType != null || this.publishType === 'prod')
            },
            {
                text: this.$t('btn_prod'),
                tooltip: this.$t('tooltip_publish_production'),
                value: 'prod',
                disabled: (this.closedType != null)
            }
        ]
    },
    computed: {
        enabled: function () {
            return (!this.closedType && this.publishType !== 'prod' && this.publishTypeSelected && this.publishTypeSelected !== this.publishType)
        },
        loading: {
            set: function (value) {
                this.$store.commit('setLoading', value)
            },
            get: function () {
                return this.$store.state.loading
            }
        }
    },
    methods: {
        loadData: function () {
            this.loading = true
            this.$axios.$get(`/admin/promos/${this.id}`)
                .then(res => {
                    this.loading = false
                    if (res.success) {
                        var data = res.data
                        this.name = data.name
                        this.closedType = data.closedType
                        this.publishType = data.publishType
                        this.publishTypeSelected = data.publishType
                        this.publishOptions = [
                            {
                                text: this.$t('btn_dev'),
                                tooltip: this.$t('tooltip_publish_development'),
                                value: null,
                                disabled: (this.closedType != null || this.publishType === 'test' || this.publishType === 'prod')
                            },
                            {
                                text: this.$t('btn_test'),
                                tooltip: this.$t('tooltip_publish_test'),
                                value: 'test',
                                disabled: (this.closedType != null || this.publishType === 'prod')
                            },
                            {
                                text: this.$t('btn_prod'),
                                tooltip: this.$t('tooltip_publish_production'),
                                value: 'prod',
                                disabled: (this.closedType != null)
                            }
                        ]
                    } else {
                        this.$router.push('/ops#404')
                    }
                })
                .catch(err => {
                    this.loading = false
                    console.error(err)
                    if (err.response.status === 401) {
                        this.$router.push('/ops#401')
                    } else {
                        this.$router.push('/ops#500')
                    }
                })
        },
        showMessage: function (_success, _data) {
            this.message = {
                success: _success,
                data: Array.isArray(_data)
                    ? _data
                    : [_data]
            }
        },
        hideMessage: function () {
            this.message = {
                success: true,
                data: []
            }
        },
        btnPublish: function () {
            let message = ''
            if (this.publishTypeSelected === 'prod') {
                message = this.$t('text_confirm_publish_prod')
            } else if (this.publishTypeSelected === 'test') {
                message = this.$t('text_confirm_publish_test')
            }

            if (message) {
                this.$refs.confirm.show(this.$t('label_atention'), message).then(result => {
                    if (result) {
                        this._publish()
                    }
                })
            }
        },
        _publish: function () {
            this.loading = true
            this.loadingPublish = true
            this.hideMessage()
            this.$axios.$put(`/admin/promos/${this.id}/publish`, { type: this.publishTypeSelected })
                .then(res => {
                    this.loading = false
                    this.loadingPublish = false
                    if (res.success) {
                        //this.loadData()
                        //this.showMessage(true, res.message || this.$t('success_alter_data'))
                        if(window) window.location.reload(true)
                    } else if (res.message) {
                        this.showMessage(false, res.message || [])
                        this.$scrollTo(".s-admin-promo-status-publish")
                    }
                })
                .catch(err => {
                    this.loading = false
                    this.loadingPublish = false
                    console.error(err)
                    this.showMessage(false, this.$t('error_alter_data'))
                    this.$scrollTo(".s-admin-promo-status-publish")
                })
        }
    }
}