export default {
    name: "SRandCoin",
    data: function () {
        return {
            tempResult: 0,
            result: 0 //0 = question, 1 = face, 2 = crown
        }
    },
    computed: {
        resultText: function () {
            if(this.result == 2) {
                return this.$t('label_crown')
            } else if(this.result == 1) {
                return this.$t('label_face')
            } else {
                return ''
            }
        },
        image: function () {
            return '/img/coins.png'
        },
        imageClass: function () {
            if(this.tempResult == 0) {
                return 'img-coin'                 
            }
            else if(this.result == 2) {
                return 'img-crown'
            } 
            else if(this.result == 1) {
                return 'img-face'  
            } 
            else {
                return 'img-coin'                 
            }                       
        },
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        }
    },
    methods: {        
        btnPerform: function () {
            if(this.isLoading) return
            this.perform()
            this.$ga.event('Random Button', 'click', 'Coin Button')
        },
        imagePerform: function () {
            if(this.isLoading) return
            this.perform()
            this.$ga.event('Random Button', 'click', 'Coin Image')
        },
        perform: function () {    
            if(!this.loading){
                this.loading = true
                setTimeout(()=>{
                    this.loading = false    
                }, 1000)
                //this.result = 0    
                this.tempResult = 0
                setTimeout(()=>{
                    this.tempResult = this.getRandom(1, 2)
                    this.result = this.tempResult
                }, 900)    
            }
        },
        getRandom: function (min, max) {
            min = parseInt(min)
            max = parseInt(max)
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
}