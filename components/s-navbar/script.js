import authUtils from '@/utils/auth-utils'
import firebase from 'firebase/app'
import 'firebase/auth'

export default {
  name: 'SNavbar',
  props: ['backbutton', 'title'],
  data: function() {
    return {
      picture: '',
      userId: '',
      name: '',
      email: '',
      user: {}
    }
  },
  computed: {
    loading: {
      set: function(value) {
        this.$store.commit('setLoading', value)
      },
      get: function() {
        return this.$store.getters.loading
      }
    },
    locales() {
      return this.$store.state.locales
    },
    locale() {
      return this.$store.state.locale
    },
    imageClass: function() {
      //return (this.loading) ? 'anime-rotate-fast' : ''
      return ''
    },
    authenticated: function() {
      const auth = authUtils.check(this.$store.getters.token)
      if (auth) {
        this.user = this.$store.getters.user
        if (this.user) {
          this.name = this.user.name || ''
          this.email = this.user.email || ''
          this.picture = this.user.picture || ''
          this.userId = this.user.uid || ''
        }
      }
      return auth
    },
    formatedName: function() {
      const name = this.name ? this.name.split(' ')[0] : ''
      return name && name.length > 10 ? name.substr(0, 10) + '...' : name
    }
  },
  methods: {
    switchLocale: function(locale) {
      if (locale !== this.$store.state.locale) {
        this.$store.commit('setLocale', locale)
        if (window) {
          window.location.reload()
        }
      }
    },
    btnLogout: function() {
      firebase
        .auth()
        .signOut()
        .then(() => {
          this.$store.commit('setToken', null)
          this.$store.commit('setUser', null)
          this.$ga.event('Logout Button', 'click', 'Navbar')
          this.$router.push('/', () => {
            if (window) window.location.reload(true)
          })
        })
    }
  }
}
