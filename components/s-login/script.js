import authUtils from '@/utils/auth-utils'
import firebase from 'firebase/app'
import 'firebase/auth'

export default {
    name: 'SLogin',
    data: function () {
        return {
            errors: [],
            loadingLogin: false,
            loadingFacebook: false,
            loadingGoogle: false
        }
    },
    created: function () {
        this.loading = true
        this.loadingLogin = true
        firebase.auth().getRedirectResult().then(authResult => {

            this.loadingLogin = false
            if (authResult) {

                if (authResult.credential) {
                    const providerId = authResult.credential.providerId
                    if (providerId === 'google.com') {
                        this.loadingGoogle = true
                    } else if (providerId === 'facebook.com') {
                        this.loadingFacebook = true
                    }

                    this.getIdToken().then(idTokenResult => {
                        this.login(idTokenResult).then(loginResult => {
                            this.loading = false
                            this.loadingGoogle = false
                            this.loadingFacebook = false
                            const target = this.$route.query.t || '/admin/p/my'
                            this.$router.push(target, () => { if (window) window.location.reload(true) })     
                        }).catch(err => {
                            console.error(err)
                            this.loading = false
                            this.loadingGoogle = false
                            this.loadingFacebook = false
                            this.errors.push(this.$t('error_login'))
                        })
                    }).catch(err => {
                        console.error(err)
                        this.loading = false
                        this.loadingGoogle = false
                        this.loadingFacebook = false
                        this.errors.push(this.$t('error_login'))
                    })
                } else {
                    this.loading = false
                    this.loadingGoogle = false
                    this.loadingFacebook = false
                }

            } else {
                this.loading = false
                this.loadingGoogle = false
                this.loadingFacebook = false
            }
        }).catch(err => {
            console.error(err)
            this.loadingLogin = false
            this.loading = false
            this.loadingGoogle = false
            this.loadingFacebook = false
            this.errors.push(this.$t('error_login'))
        })
    },
    computed: {
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        }
    },
    methods: {
        btnLoginGoogle: function () {
            this.errors = []
            let provider = new firebase.auth.GoogleAuthProvider()
            //provider.addScope('https://www.googleapis.com/auth/user.birthday.read')
            provider.setCustomParameters({ prompt: 'select_account' })
            firebase.auth().useDeviceLanguage()
            this.loading = true
            this.loadingGoogle = true
            this.loadingFacebook = false
            firebase.auth().signInWithRedirect(provider)
            this.$ga.event('Login Button', 'click', 'Google')
        },
        btnLoginFacebook: function () {
            this.errors = []
            let provider = new firebase.auth.FacebookAuthProvider()
            //provider.addScope('age_range')
            provider.setCustomParameters({ prompt: 'select_account' })
            firebase.auth().useDeviceLanguage()
            this.loading = true
            this.loadingFacebook = true
            this.loadingGoogle = false
            firebase.auth().signInWithRedirect(provider)
            this.$ga.event('Login Button', 'click', 'Facebook')
        },
        login: function (idToken) {
            const self = this
            return new Promise((resolve, reject) => {
                this.$axios.$post('/auth', { 'firebaseToken': idToken })
                    .then(result => {
                        if (result.success) {
                            var data = result.data
                            this.$store.commit('setToken', data.token)
                            this.$store.commit('setUser', data.user)
                            resolve(true)
                        } else {
                            authUtils.logout()
                            this.$store.commit('setToken', '')
                            this.$store.commit('setUser', null)
                            resolve(false)
                        }
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        getIdToken: function () {
            return new Promise((resolve, reject) => {
                var user = firebase.auth().currentUser
                if (user) {
                    user.getIdToken(true).then(idToken => {
                        resolve(idToken)
                    }).catch(err => {
                        reject(err)
                    })
                } else {
                    reject(this.$t('error_user_unauthenticated'))
                }
            })
        }

    }
}