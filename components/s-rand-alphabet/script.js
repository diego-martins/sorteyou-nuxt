export default {
    name: 'SRandAlphabet',
    data: function () {
        return {
            qtd: 1,
            minRequired: 1,
            maxRequired: 26,
            isLoading: false,
            errors: [],
            results: [],
            options: [],
            values: [],
            order: null
        }
    },
    created: function () {
        for(let i=65; i<=90; i++){
            this.values.push(String.fromCharCode(i))
            this.options.push(String.fromCharCode(i))
        }
    },
    computed: {
        resultText: function () {
            let _return = ''
            let list = this.results
            if(this.order){
                list = _.orderBy(list, 'value', this.order)
            }
            let count = list.length
            for (let i = 0; i < count; i++) {
                if (_return) {
                    _return += ' - '
                }
                _return += list[i].value
            }
            return _return
        },
        orderIcon: function () {
            if(!this.order) {
                return 'fa-sort-alpha-down'
            } else if(this.order === 'asc') {
                return 'fa-sort-alpha-up'
            } else if (this.order === 'desc') {
                return 'fa-random'
            } else {
                return ''
            }
        }
    },
    methods: {
        isValid: function () {
            this.errors = []
            this.results = []

            this.qtd = parseInt(this.qtd||'0')
            this.maxRequired = this.values.length

            if(this.values == 0){
                this.errors.push(this.$t('error_letter_required'))
            }
            else if (this.qtd < this.minRequired || this.qtd > this.maxRequired) {
                this.errors.push(this.$t('error_qtd'))
            }    

            return (this.errors.length == 0)
        },
        btnPerform: function () {
            if(this.isLoading) return
            this.$store.commit('setLoading', true)
            this.isLoading = true
            this.$ga.event('')
            setTimeout(()=>{
                if (this.isValid()) {
                    this.results = []
                    
                    while (this.results.length < this.qtd) {
                        const index = this.getRandom(0, this.values.length - 1)
                        const value = this.values[index]
                        let has = false
                        for (let i = 0; i < this.results.length; i++) {
                            if (this.results[i].value == value) {
                                has = true
                                break
                            }
                        }
                        if (!has) {
                            this.results.push({value: value})
                        }
                    }
                    
                }
                this.$store.commit('setLoading', false)
                this.isLoading = false    
            }, 500)
            this.$ga.event('Random Button', 'click', 'Alphabet')
        },
        getRandom: function (min, max) {
            min = parseInt(min)
            max = parseInt(max)
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        btnOrder: function () {
            if(!this.order) {
                this.order = 'asc'
            }
            else if(this.order === 'asc') {
                this.order = 'desc'
            }
            else if(this.order === 'desc') {
                this.order = null
            }
            this.$ga.event('Order Button', 'click', 'Alphabet')
        }
    }
}