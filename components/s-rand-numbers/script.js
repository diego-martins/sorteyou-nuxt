import _ from 'lodash'

export default {
    name: 'SRandNumbers',
    data: function () {
        return {
            minValDefault: 1,
            maxValDefault: 100,
            qtdDefault: 1,
            minValRequired: 0,
            maxValRequired: 100000,
            minQtdRequired: 1,
            maxQtdRequired: 100000,
            minVal: 0,
            maxVal: 0,
            qtd: 0,
            isLoading: false,
            errors: [],
            results: [],
            order: null
        }
    },
    created: function () {
        this.minVal = this.minValDefault
        this.maxVal = this.maxValDefault
        this.qtd = this.qtdDefault
    },
    computed: {
        resultText: function () {
            let _return = ''
            let list = this.results
            if(this.order){
                list = _.orderBy(list, 'value', this.order)
            }
            let count = list.length
            for (let i = 0; i < count; i++) {
                if (_return) {
                    _return += ' - '
                }
                _return += list[i].value
            }
            return _return
        },
        orderIcon: function () {
            if(!this.order) {
                return 'fa-sort-numeric-down'
            } else if(this.order === 'asc') {
                return 'fa-sort-numeric-up'
            } else if (this.order === 'desc') {
                return 'fa-random'
            } else {
                return ''
            }
        }
    },
    methods: {
        isValid: function () {
            this.errors = []
            this.results = []
            this.minVal = parseInt(this.minVal||'0')
            this.maxVal = parseInt(this.maxVal||'0')
            this.qtd = parseInt(this.qtd||'0')
            this.maxQtdRequired = this.maxVal - this.minVal + 1

            //if(this.minVal == this.maxVal){
            //    this.errors.push(this.$t('error_vals_equals'))
            //}

            if (this.minVal < this.minValRequired || this.minVal > this.maxValRequired) {
                this.errors.push(this.$t('error_val_initial_between', {min: this.minValRequired, max: this.maxValRequired}))
            }

            if (this.maxVal < this.minValRequired || this.maxVal > this.maxValRequired) {
                this.errors.push(this.$t('error_val_final_between', {min: this.minValRequired, max: this.maxValRequired}))
            }

            if(this.errors.length == 0){
                if(this.minVal > this.maxVal){
                    this.errors.push(this.$t('error_val_initial_greater'))
                }
                else if(this.qtd > this.maxVal){
                    this.errors.push(this.$t('error_qtd_greater_final'))
                }
                else if (this.qtd < this.minQtdRequired || this.qtd > this.maxQtdRequired) {
                    this.errors.push(this.$t('error_qtd'))
                }    
            }

            return (this.errors.length == 0)
        },
        btnPerform: function () {
            if(this.isLoading) return
            this.$store.commit('setLoading', true)
            this.isLoading = true
            setTimeout(()=>{
                if (this.isValid()) {
                    this.results = []
                    while (this.results.length < this.qtd) {
                        const value = this.getRandom(this.minVal, this.maxVal)
                        let has = false
                        for (let i = 0; i < this.results.length; i++) {
                            if (this.results[i].value == value) {
                                has = true
                                break
                            }
                        }
                        if (!has) {
                            this.results.push({value: value})
                        }
                    }
                }
                this.$store.commit('setLoading', false)
                this.isLoading = false    
            }, 500)
            this.$ga.event('Random Button', 'click', 'Numbers')
        },
        getRandom: function (min, max) {
            min = parseInt(min)
            max = parseInt(max)
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        btnOrder: function () {
            if(!this.order) {
                this.order = 'asc'
            }
            else if(this.order === 'asc') {
                this.order = 'desc'
            }
            else if(this.order === 'desc') {
                this.order = null
            }
            this.$ga.event('Order Button', 'click', 'Numbers')
        }
    }
}