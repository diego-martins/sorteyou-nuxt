import browser from 'browser-detect'

export default ({ isHMR, req, route, redirect, error }) => {

    if (isHMR) return
    
    const agentString = (process.server)
        ? req.headers['user-agent']
        : navigator.userAgent

    const agent = browser(agentString)
    const _redirect = agent && agent.name === 'ie' && agent.versionNumber <= 11

    if (_redirect && route.path !== '/browser-off') {
        return redirect('/browser-off')
    } else if (!_redirect && route.path === '/browser-off') {
        return error({ statusCode: 404 })
    }

}