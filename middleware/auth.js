export default (ctx) => {
    if(!ctx.store.getters.isAuthenticated) {
        if(ctx.req && ctx.req.url) {
            return ctx.redirect(`/login?t=${ctx.req.url}`)
        }
        return ctx.redirect(`/login`)
    }
}