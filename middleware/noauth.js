export default (ctx) => {
    if(ctx.store.getters.isAuthenticated) {
        return ctx.redirect('/')
    }
}