import cookie from 'cookie'

export default function({ isHMR, app, store, route, req }) {
  if (isHMR) return

  if (req && route.name) {
    let locale
    if (req.headers.cookie) {
      const parsed = cookie.parse(req.headers.cookie)
      locale = parsed.locale
    }

    if (!locale) {
      locale = req.headers['accept-language']
        .split(',')[0]
        .toLocaleLowerCase()
        .substring(0, 2)
    }

    if (locale !== app.i18n.locale) {
      store.commit('setLocale', locale)
      app.i18n.locale = store.state.locale
    }
  }
}
