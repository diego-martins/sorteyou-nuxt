FROM node:8.11.3-alpine
RUN npm install pm2 -g
RUN mkdir -p /app
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build
ENV HOST 0.0.0.0
EXPOSE 3000
#CMD ["npm", "start"]
#EXPOSE 80 443
CMD ["pm2-runtime", "start", "process.json"]