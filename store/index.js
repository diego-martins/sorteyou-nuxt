import Vue from 'vue'
import Vuex from 'vuex'
import { version } from '@/package.json'
import authUtils from '@/utils/auth-utils'
import Cookies from 'js-cookie'
import cookie from 'cookie'

Vue.use(Vuex)

const state = {
  version: version,
  locales: [
    {
      code: 'pt',
      name: 'Português'
    },
    {
      code: 'en',
      name: 'English'
    }
  ],
  locale: 'pt',
  token: '',
  user: null,
  loading: false,
  subPromoId: '',
  admPromo: null,
  admPromoLoading: false,
  admCredits: null,
  admCreditsLoading: false
}

const getters = {
  isAuthenticated: state => authUtils.check(state.token),
  loading: state => state.loading,
  user: state => state.user,
  token: state => state.token,
  subPromoId: state => state.subPromoId,
  admPromo: state => state.admPromo,
  admPromoLoading: state => state.admPromoLoading,
  admCredits: state => state.admCredits,
  admCreditsLoading: state => state.admCreditsLoading
}

const mutations = {
  setLocale: (state, locale) => {
    if (state.locales.find(item => item.code === locale)) {
      state.locale = locale
    }
  },
  setToken: (state, token) => {
    state.token = token || null
  },
  setUser: (state, user) => {
    state.user = user || null
  },
  setLoading: (state, loading) => {
    state.loading = loading
  },
  setSubPromoId: (state, subPromoId) => {
    state.subPromoId = subPromoId
  },
  setAdmPromo: (state, admPromo) => {
    state.admPromo = admPromo
  },
  setAdmPromoLoading: (state, loading) => {
    state.admPromoLoading = loading
  },
  setAdmCredits: (state, admCredits) => {
    state.admCredits = admCredits
  },
  setAdmCreditsLoading: (state, loading) => {
    state.admCreditsLoading = loading
  }
}

const actions = {
  nuxtServerInit: (store, context) => {
    let parsed = {}
    if (context.req.headers.cookie) {
      parsed = cookie.parse(context.req.headers.cookie)
    }
    store.commit('setToken', parsed.token || null)
    store.commit('setUser', parsed.user ? JSON.parse(parsed.user) : null)
    store.commit('setSubPromoId', parsed.subPromoId || null)
  },
  async loadAdmPromo({ commit }, params) {
    commit('setAdmPromoLoading', true)
    await this.$axios.get(`/admin/promos/${params.id}`).then(res => {
      if (res.status === 200) {
        const data = res.data.data
        commit('setAdmPromo', data)
        commit('setAdmPromoLoading', false)
      }
    })
  },
  async loadAdmCredits({ commit }) {
    commit('setAdmCreditsLoading', true)
    await this.$axios.get(`/admin/payments/credits`).then(res => {
      if (res.status === 200) {
        const data = res.data.data
        commit('setAdmCredits', data.credits || 0)
        commit('setAdmCreditsLoading', false)
      }
    })
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})

const setCookie = (key, val) => {
  if (val !== null && val !== undefined) {
    Cookies.set(key, val, { expires: 365 })
  } else {
    Cookies.remove(key)
  }
}

store.subscribe((mutation, state) => {
  setCookie('locale', state.locale)

  setCookie('token', state.token)

  setCookie('user', state.user ? JSON.stringify(state.user) : null)

  setCookie('version', state.version)

  setCookie('subPromoId', state.subPromoId)
})

export default () => store
