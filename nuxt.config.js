const nodeExternals = require('webpack-node-externals')
const projectPackage = require('./package.json')

const siteUrl = 'https://www.sorteyou.com'
const apiUrl =
  process.env.NODE_ENV === 'production'
    ? 'https://sorteyou.com:8000'
    : 'http://192.168.15.11:3000'

module.exports = {
  env: {
    siteUrl,
    apiUrl
  },
  head: {
    titleTemplate: titleChunk =>
      titleChunk ? `${titleChunk} - Sorteyou` : 'Sorteyou',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Promotor de sorteios e promoções'
      },
      { hid: 'theme-color', name: 'theme-color', content: '#9C27B0' },
      { hid: 'robots', name: 'robots', content: 'index, follow' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Sorteyou' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:url', property: 'og:url', content: siteUrl },
      { hid: 'fb:app_id', property: 'fb:app_id', content: '169724953840724' },
      { hid: 'twitter:site', property: 'twitter:site', content: '@sorteyou' },
      {
        name: 'google-site-verification',
        content: 'nkZ7zEyXY_qchR1PhaR5S_if6xIl06RU-JpdvnHe1zs'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: siteUrl + '/img/logo-sorteyou.jpg'
      },
      { hid: 'og:image:width', property: 'og:image:width', content: '640' },
      { hid: 'og:image:height', property: 'og:image:height', content: '480' },
      {
        hid: 'twitter:image',
        property: 'twitter:image',
        content: siteUrl + '/img/logo-sorteyou.jpg'
      },
      {
        hid: 'twitter:card',
        property: 'twitter:card',
        content: 'summary_large_image'
      },
      {
        hid: 'itemprop:image',
        itemprop: 'image',
        content: siteUrl + '/img/logo-sorteyou.jpg'
      }
    ],
    link: [
      { rel: 'icon', href: '/favicon.ico', type: 'image/x-icon' },
      { rel: 'icon', href: '/favicon-16.png', sizes: '16x16' },
      { rel: 'icon', href: '/favicon-32.png', sizes: '32x32' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:100:300,400,500,700,900|Material+Icons'
      },
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
        integrity:
          'sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt',
        crossorigin: 'anonymous'
      }
    ]
  },
  loading: { color: '#9C27B0' },
  router: {
    middleware: ['i18n', 'user-agent']
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/sitemap',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-116905621-1'
      }
    ],
    [
      'nuxt-robots-module',
      {
        UserAgent: '*',
        Disallow: ''
      }
    ]
  ],
  sitemap: {
    exclude: [
      '/admin/**',
      '/profile',
      '/profile/*',
      '/p/*',
      '/error',
      '/ops',
      '/browser-off',
      '/terms'
    ]
  },
  plugins: [
    '~/plugins/i18n',
    '~/plugins/axios',
    '~/plugins/vuetify',
    '~/plugins/vue-clipboards',
    '~/plugins/firebase',
    '~/plugins/vue-scrollto',
    '~/plugins/vue-social-sharing',
    '~/plugins/filters',
    { src: '~/plugins/vue-progressive-image', ssr: false }
  ],
  build: {
    vendor: [
      'vue-i18n',
      'babel-polyfill',
      'vuetify',
      'vue-clipboards',
      'vue-social-sharing',
      'vue-scrollto',
      'browser-detect'
    ],
    extractCSS: true,
    babel: {
      presets: [
        [
          'vue-app',
          {
            targets: {
              browsers: projectPackage.browserslist
            }
          }
        ]
      ],
      plugins: [
        [
          'transform-imports',
          {
            vuetify: {
              transform: 'vuetify/es5/components/${member}',
              preventFullImport: true
            }
          }
        ]
      ]
    },
    extend(config, { isDev, isClient, isServer }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      if (isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/]
          })
        ]
      }
    }
  }
}
