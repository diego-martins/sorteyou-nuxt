import jwtDecode from 'jwt-decode'
import moment from 'moment'

export default {
    check: function (token) {
        if (!token) {
            return false
        }
        try {
            const decoded = jwtDecode(token)
            const exp = (decoded && decoded.exp) ? decoded.exp : 0
            if(exp === 0) {
                return true
            }
            const expireAt = moment(exp * 1000)
            if (moment().isAfter(expireAt)) {
                return false
            }
        } catch (ex) {
            console.error(ex)
            return false
        }
        return true
    }
}