import _ from 'lodash'
import alert from '@/components/alert.vue'

export default {
    name: 'listPromo',
    components: { alert },
    data: function () {
        return {
            list: [],
            createLoading: false
        }
    },
    middleware: 'auth',
    head() {
        return {
            title: this.$t('page.admin.p.my.title'),
            meta: [
                { hid: 'robots', name: 'robots', content: 'noindex, nofollow' }
            ]
        }
    },
    asyncData({ app, error }) {

        return app.$axios.$get('/admin/promos/my')
            .then(res => {
                if (res.success) {
                    let data = {}
                    data.list = []
                    res.data.forEach(item => {
                        data.list.push({
                            id: item.id,
                            performAt: item.performAt,
                            name: item.name,
                            publishType: item.publishType,
                            closedType: item.closedType
                        })
                    })

                    if (data.list.length > 0) {
                        data.list = _.orderBy(data.list, 'performAt', 'desc')
                    }

                    return data
                } else {
                    error({
                        statusCode: 404
                    });
                }
            })
            .catch(err => {
                error({
                    statusCode: err.response ? err.response.status : 500,
                    message: err.response ? err.response.data : ""
                });
            });
    },
    computed: {
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        }
    },
    methods: {
        btnCreate() {
            this.createLoading = true
            this.$axios.$get('/admin/promos/canCreate')
                .then(res => {
                    this.createLoading = false
                    if (res.success) {
                        this.$router.push("/admin/p/create")
                    } else {
                        this.$refs.alert.showError('Ops', res.message)
                    }
                })
                .catch(err => {
                    this.createLoading = false
                    console.error(err)
                })
        }
    }
}