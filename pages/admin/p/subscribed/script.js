import moment from 'moment'
import _ from 'lodash'
import alert from '@/components/alert'
import confirm from '@/components/confirm'

export default {
    name: 'AdminPromoSubscribed',
    components: { alert, confirm },
    data: function () {
        return {
            list: [],
            deleteItem: null,
            dialogTitle: null,
            dialogMessage: null,
            dialogDeletedMessage: {
                show: false,
                title: '',
                info: '',
                message: ''
            }
        }
    },
    middleware: 'auth',
    head() {
        return {
            title: this.$t('page.admin.p.subscribed.title'),
            meta: [
                { hid: 'robots', name: 'robots', content: 'noindex, nofollow' }
            ]
        }
    },
    asyncData({ app, error }) {
        return app.$axios.$get('/admin/promos/subscribed')
            .then(res => {

                if (res.success) {
                    let data = {}
                    data.list = []

                    res.data.forEach(item => {

                        data.list.push({
                            id: item.id,
                            performAt: item.performAt,
                            name: item.name,
                            publishType: item.publishType,
                            closedType: item.closedType,
                            winner: item.winner,
                            finished: (item.finishAt)
                                ? moment().isAfter(item.finishAt)
                                : false
                        })
                    })

                    if (data.list.length > 0) {
                        data.list = _.orderBy(data.list, 'performAt', 'desc')
                    }

                    return data

                } else {
                    error({
                        statusCode: 404
                    });
                }
            })
            .catch(err => {
                error({
                    statusCode: err.response ? err.response.status : 500,
                    message: err.response ? err.response.data : ""
                });
            });
    },
    computed: {
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        }
    },
    methods: {
        loader: function () {
            this.loading = true
            this.$axios.$get('/admin/promos/subscribed')
                .then(res => {
                    this.loading = false
                    if (res.success) {
                        var data = res.data
                        this.list = []

                        data.forEach(item => {

                            this.list.push({
                                id: item.id,
                                performAt: item.performAt,
                                name: item.name,
                                publishType: item.publishType,
                                closedType: item.closedType,
                                winner: item.winner,
                                finished: (item.finishAt)
                                    ? moment().isAfter(item.finishAt)
                                    : false
                            })
                        })

                        if (this.list.length > 0) {
                            this.list = _.orderBy(this.list, 'performAt', 'desc')
                        }
                    }
                })
                .catch(err => {
                    this.loading = false
                    console.error(err)
                })
        },
        showDialog: function (title, message) {
            if (title && !message) {
                this.dialogTitle = null
                this.dialogMessage = title
                this.$refs.localDialog.show()
            } else if (title && message) {
                this.dialogTitle = title
                this.dialogMessage = message
                this.$refs.localDialog.show()
            }
        },
        btnShowGoodMessage: function (id) {
            const item = this.list.find(item => item.id === id)
            if (item) {
                this.$refs.alert.showSuccess(
                    this.$t('label_congrats'),
                    this.$t('success_you_won', { position: item.winner.wonPosition })
                )
            }
        },
        btnShowBadMessage: function (id) {
            const item = this.list.find(item => item.id === id)
            if (item) {
                this.dialogDeletedMessage.title = this.$t('label_sorry')
                this.dialogDeletedMessage.info = this.$t('error_you_lost', { position: item.winner.wonPosition })
                this.dialogDeletedMessage.message = item.winner.deletedText
                this.dialogDeletedMessage.show = true
            }
        },
        btnDelete: function (id) {
            const deleteItem = this.list.find(item => item.id === id)
            if (deleteItem) {
                this.$refs.confirm.show(
                    this.$t('label_confirm'),
                    this.$t('text_confirm_delete_promo_user', { name: deleteItem.name }),
                    { color: 'red', html: true }
                ).then(confirmResult => {
                    if (confirmResult) {
                        this._deletePromo(id)
                    }
                })
            }
        },
        _deletePromo: function (id) {
            this.loading = true
            this.$axios.$delete(`/promos/${id}/user`)
                .then(res => {
                    this.loading = false
                    if (res.success) {
                        this.loader()
                    } else if (res.message) {
                        this.$refs.alert.showError('Ops', res.message)
                    }
                })
                .catch(err => {
                    this.loading = false
                    console.error(err)
                })
        },
        handlerConfirmDelete: function (item) {
            this.deleteItem = item
            this.$refs.modalConfirmDelete.show()
        },
        handlerDelete: function () {
            this.loading = true
            this.$axios.$delete(`/promos/${this.deleteItem.id}/user`)
                .then(res => {
                    this.loading = false
                    if (res.success) {
                        this.loader()
                    } else if (res.message) {
                        this.showDialog(res.message)
                    }
                })
                .catch(err => {
                    this.loading = false
                    console.error(err)
                })
        }
    }
}