import moment from 'moment'
import url from 'url'
import alert from '@/components/alert'
import confirm from '@/components/confirm'
import SNavbar from "@/components/s-navbar"

export default {
    name: 'AdminPromoCreate',
    components: { SNavbar, alert, confirm },
    layout: 'no-navbar',
    data: function () {
        return {
            id: '',
            form: {
                //step 1
                name: '',
                country: '',
                desc: '',
                //step 2
                startDate: null,
                finishDate: null,
                performDate: null,
                //step 3
                file: null,
                videoId: '',
                fakeImage: false,
                imageUrl: null,
                loading: false,
                valid: {
                    form1: false,
                    form2: false,
                    form3: false
                },
                rules: {
                    name: [
                        v => !!v || this.$t('error_field_required'),
                        v => (v && v.length <= 50) || this.$t('error_field_max_length', { length: 50 })
                    ],
                    desc: [
                        v => !!v || this.$t('error_field_required'),
                        v => (v && v.length <= 500) || this.$t('error_field_max_length', { length: 500 })
                    ],
                    startDate: [
                        v => !!v || this.$t('error_field_required')
                    ],
                    finishDate: [
                        v => !!v || this.$t('error_field_required')
                    ],
                    performDate: [
                        v => !!v || this.$t('error_field_required')
                    ]
                }
            },
            modalStartDate: false,
            modalFinishDate: false,
            modalPerformDate: false,
            localFileName: '',
            localFileUrl: '',
            mediaOptions: [],
            mediaType: 'image',
            dialogCompleted: false,
            step: 1,
            saved: false
        }
    },
    middleware: 'auth',
    head() {
        return {
            title: this.$t('page.admin.p.create.title'),
            meta: [
                { hid: 'robots', name: 'robots', content: 'noindex, nofollow' }
            ]
        }
    },
    created: function () {
        if (this.$store.getters.lang === 'pt') {
            this.form.country = 'BR'
        } else {
            this.form.country = 'US'
        }
        this.mediaOptions = [
            { text: this.$t('label_image'), value: 'image' },
            { text: this.$t('label_video'), value: 'video' }
        ]
    },
    computed: {
        hasChanges: function () {
            return (
                !!this.form.name ||
                !!this.form.desc ||
                !!this.form.startDate ||
                !!this.form.finishDate ||
                !!this.form.performDate ||
                (this.mediaType === 'video' && !!this.form.videId) ||
                (this.mediaType === 'image' && !!this.form.file)
            )
        },
        startDateFormatted: function () {
            if (this.form.startDate && this.form.startDate.length === 10) {
                return moment(this.form.startDate).format(this.$t('date_format'))
            }
        },
        finishDateFormatted: function () {
            if (this.form.finishDate && this.form.finishDate.length === 10) {
                return moment(this.form.finishDate).format(this.$t('date_format'))
            }
        },
        performDateFormatted: function () {
            if (this.form.performDate && this.form.performDate.length === 10) {
                return moment(this.form.performDate).format(this.$t('date_format'))
            }
        },
        lang: function () {
            return this.$store.state.lang || 'pt'
        },
        videoUrl: {
            get: function () {
                return this.form.videoId
            },
            set: function (value) {
                const _value = this.getVideIdFromUrl(value)
                if (_value) {
                    this.form.videoId = _value
                } else {
                    this.form.videoId = value
                }
            }
        },
        imageUrlPerform: function () {
            if (this.mediaType === 'video') {
                if (this.form.videoId) {
                    return `https://img.youtube.com/vi/${this.form.videoId}/sddefault.jpg`
                }
                return '/img/no-video.jpg'
            }
            if (this.localFileUrl) {
                return this.localFileUrl
            } else if (this.form.imageUrl) {
                return this.form.imageUrl
            }
            return '/img/no-image.jpg'
        }
    },
    methods: {
        getVideIdFromUrl: function (value) {
            let videId = ''
            if (value) {
                const domains = ['youtu.be', 'youtube.com', 'www.youtube.com', 'm.youtube.com']
                const _url = url.parse(value)
                if (_url && _url.hostname && domains.includes(_url.hostname)) {
                    let _value = ''
                    if (_url.query) {
                        _value = _url.query.split('&')
                            .map(item => (item) ? item.split('=') : '')
                            .find(item => item && item.length === 2 && item[0] === 'v')
                        _value = (_value && _value.length === 2) ? _value[1] : ''
                    }
                    else if (_url.pathname) {
                        _value = _url.pathname.split('/')
                        _value = (_value && _value.length > 0) ? _value[_value.length - 1] : ''
                    }
                    if (_value) {
                        videId = _value
                    }
                }
            }
            return videId
        },
        pickFile() {
            this.$refs.file.click()
        },
        onFilePicked(e) {
            const files = e.target.files
            if (files[0] !== undefined) {
                this.localFileName = files[0].name
                if (this.localFileName.lastIndexOf('.') <= 0) {
                    return
                }
                const fr = new FileReader()
                fr.readAsDataURL(files[0])
                fr.addEventListener('load', () => {
                    this.localFileUrl = fr.result
                    this.form.file = files[0]
                })
                this.form.file = files[0]
            } else {
                this.file = null
                this.localFileName = ''
                this.localFileUrl = ''
            }
        },
        btnCancel: function () {
            this.$router.push('/admin/p/my')
            //TODO: remove
            /*
            this.$refs.confirm.show(
                this.$t('label_cancel'), 
                this.$t('text_confirm_cancel_create_promo'))
                .then(resultConfirm => {
                    if(resultConfirm) {
                        this.$router.push('/admin/p/my')
                    }
                })
            */
        },
        btnBackStep: function () {
            if (this.step > 1) this.step--
        },
        btnNextStep: function () {
            if (this.step === 1 && this.$refs.form1.validate()) {
                this.step++
            } else if (this.step === 2 && this.$refs.form2.validate()) {
                let _error = ''
                if (moment(this.form.startDate).isSame(moment(this.form.finishDate).toDate())) {
                    _error = this.$t('error_dates_cannot_equals')
                }
                else if (moment(this.form.startDate).isAfter(moment(this.form.finishDate))) {
                    _error = this.$t('error_dates_order')
                }
                else if (moment(this.form.finishDate).isSameOrBefore(moment())) {
                    _error = this.$t('error_date_finish_after_now')
                }
                else if (moment(this.form.finishDate).isSameOrAfter(moment(this.form.performDate))) {
                    _error = this.$t('error_date_finish_after_perform')
                }
                else if (moment(this.form.performDate).isAfter(moment(this.form.finishDate).add(7, 'days'))) {
                    _error = this.$t('error_date_perform_late')
                }

                if (!!_error) {
                    this.$refs.alert.showError('Ops', _error)
                } else {
                    this.step++
                }
            } else if (this.step === 3 && this.$refs.form3.validate()) {
                this._submit()
            }
        },
        btnUpdatePromo: function () {
            this.dialogCompleted = false
            this.$router.push(`/admin/p/${this.id}`, () => { if(window) window.location.reload(true) })
        },
        btnViewPromo: function () {
            this.dialogCompleted = false
            const route = this.$router.resolve(`/p/${this.id}`)
            window.open(route.href, '_blank')
            this.$router.push(`/admin/p/${this.id}`, () => { if(window) window.location.reload(true) })
        },
        _submit: function () {
            let form = this.form
            let formData = new FormData()
            //step 1
            formData.append('name', form.name)
            formData.append('country', form.country)
            formData.append('desc', form.desc)

            //step 2
            formData.append('startAt', moment(`${form.startDate} 00:00`).toJSON())
            formData.append('finishAt', moment(`${form.finishDate} 00:00`).toJSON())
            formData.append('performAt', moment(`${form.performDate} 00:00`).toJSON())

            //step 3
            if (this.mediaType === 'video') {
                formData.append('videoId', encodeURIComponent(form.videoId))
            } else {
                formData.append('file', form.file)
                formData.append('fakeImage', form.fakeImage)
            }

            let config = {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }

            this.form.loading = true
            this.$axios.$post(`/admin/promos`, formData, config)
                .then(res => {
                    this.form.loading = false
                    if (res.success) {
                        this.id = res.data.id
                        this.dialogCompleted = true
                        this.saved = true
                    } else if (res.data.errors.length) {
                        this.$refs.alert.showError('Ops', res.data.errors[0])
                    }
                })
                .catch(err => {
                    console.error(err)
                    this.form.loading = false
                    const response = err.response ? err.response.data : ''
                    this.$refs.alert.showError('Ops', response || this.$t('error_create_promo'))
                })
        }
    },
    beforeRouteLeave(to, from, next) {
        if (!this.saved && this.hasChanges) {
            this.$refs.confirm.show(
                '',
                this.$t('text_confirm_leave_unsaved'))
                .then(resultConfirm => {
                    if (resultConfirm) {
                        next()
                    } else {
                        next(false)
                    }
                })
        } else {
            next()
        }
    }
}