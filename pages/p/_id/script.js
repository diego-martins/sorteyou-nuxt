import SocialSharing from 'vue-social-sharing'
import alert from '@/components/alert.vue'
import authUtils from '@/utils/auth-utils'

export default {
    name: 'Promo',
    components: { alert, SocialSharing },
    data: function () {
        return {
            id: '',
            name: '',
            desc: '',
            terms: '',
            startAt: '',
            finishAt: '',
            performAt: '',
            imageUrl: '',
            totalUsers: 0,
            totalWinners: 0,
            minUsers: 0,
            maxUsers: 0,
            maxWinners: 0,
            closedType: null,
            closedText: null,
            owner: false,
            finished: false,
            subscribed: false,
            publishType: null,
            acceptTerms: true,
            currentUrl: '',
            winner: null,
            videoId: null,
            fakeImage: false,
            dialogSubscribed: false,
            allDesc: false,
            allTerms: false,
            overriddenNetworks: {
                whatsapp: {
                    //sharer: "https://api.whatsapp.com/send?text=@description%0D%0A%0A@url%20",
                    sharer: "https://api.whatsapp.com/send?text=@url%20",
                    type: "popup"
                }
            }
        }
    },
    asyncData({ app, params, env, error }) {
        const promoId = params.id
        return app.$axios.$get(`/promos/${promoId}`)
            .then(res => {                
                if (res.success) {
                    var responseData = res.data
                    let data = {}
                    data.id = responseData.id
                    data.currentUrl = env.siteUrl + '/p/' + data.id
                    data.name = responseData.name
                    data.desc = responseData.desc
                    data.terms = responseData.terms
                    data.minUsers = responseData.minUsers
                    data.maxUsers = responseData.maxUsers
                    data.maxWinners = responseData.maxWinners
                    data.totalUsers = responseData.totalUsers + responseData.totalWinners
                    data.totalWinners = responseData.totalWinners
                    data.startAt = responseData.startAt
                    data.finishAt = responseData.finishAt
                    data.performAt = responseData.performAt
                    data.imageUrl = responseData.imageUrl
                    data.owner = responseData.owner
                    data.finished = responseData.finished
                    data.publishType = responseData.publishType
                    data.closedType = responseData.closedType
                    data.closedText = responseData.closedText
                    data.videoId = responseData.videoId
                    data.fakeImage = responseData.fakeImage
                    return data
                } else {
                    error({
                        statusCode: 404
                    })    
                }
            })
            .catch(err => {
                error({
                    statusCode: err.response ? err.response.status : 500,
                    message: err.response ? err.response.data : ''
                })
            })
    },
    created: function () {
        this.loadCurrentUser()
    },
    mounted: function () {
        /*
        setTimeout(function(){
            let ads = document.createElement('script')    
            ads.setAttribute('src', '//native.propellerads.com/1?z=1912027&eid=p_1912027')
            ads.setAttribute('async', 'async')            
            document.getElementById('p_1912027').appendChild(ads)
        }, 1000)
        */
    },
    computed: {
        headDescription: function () {
            const text = this.trunc(this.desc, 100, true)
            return (text.length > 1) ? text[0] + text[1] : text[0]
        },
        descParts: function () {
            let _parts = []
            if (this.desc) {
                _parts = (this.allDesc) ? this.desc.split('\n') : this.trunc(this.desc, 150, true)
            }
            return _parts
        },
        termsParts: function () {
            let _parts = []
            if (this.terms) {
                _parts = (this.allTerms) ? this.terms.split('\n') : this.trunc(this.terms, 150, true)
            }
            return _parts
        },
        textSub: function () {
            if (this.closedType === 'cancel') {
                return this.$t('btn_promo_closed_cancel')
            }
            else if (this.closedType == 'success') {
                return this.$t('btn_promo_closed_success')
            }
            else if (this.finished) {
                return this.$t('btn_date_finished')
            }
            else if (this.subscribed) {
                return this.$t('btn_participating')
            }
            else if (this.owner || this.publishType !== 'prod') {
                return this.$t('btn_participate_denied')
            }
            else if (this.maxUsers && this.totalUsers >= this.maxUsers) {
                return this.$t('btn_participate_denied')
            }
            return this.$t('btn_participate')
        },
        btnSubEnabled: function () {
            return this.publishType === 'prod'
                && !this.closedType
                && !this.finished
                && !this.subscribed
                && !this.owner
                && (this.totalUsers < this.maxUsers)
        },
        loading: {
            get: function () {
                return this.$store.state.loading
            },
            set: function (value) {
                this.$store.commit('setLoading', value)
            }
        },
        imageVideoUrl: function () {
            if (this.videoId) {
                return `https://img.youtube.com/vi/${this.videoId}/sddefault.jpg`
            }
            return ''
        }
    },
    head() {
        let metas = []
        metas.push({ hid: 'description', name: 'description', content: this.headDescription })
        metas.push({ hid: 'og:title', property: 'og:title', content: this.name })
        metas.push({ hid: 'og:url', property: 'og:url', content: this.currentUrl })
        metas.push({ hid: 'og:description', property: 'og:description', content: this.headDescription })
        metas.push({ hid: 'twitter:title', property: 'twitter:title', content: this.name })
        metas.push({ hid: 'twitter:description', property: 'twitter:description', content: this.headDescription })
        metas.push({ hid: 'itemprop:name', itemprop: 'name', content: this.name })
        metas.push({ hid: 'itemprop:description', itemprop: 'description', content: this.headDescription })

        if (this.imageUrl) {
            metas.push({ hid: 'og:image', property: 'og:image', content: this.imageUrl })
            metas.push({ hid: 'og:image:width', property: 'og:image:width', content: '1024' })
            metas.push({ hid: 'og:image:height', property: 'og:image:height', content: '768' })
            metas.push({ hid: 'twitter:image', property: 'twitter:image', content: this.imageUrl })
            metas.push({ hid: 'twitter:card', property: 'twitter:card', content: 'summary_large_image' })
            metas.push({ hid: 'itemprop:image', itemprop: 'image', content: this.imageUrl })
        } else if (this.imageVideoUrl) {
            metas.push({ hid: 'og:type', property: 'og:type', content: 'video.other' })
            metas.push({ hid: 'og:image', property: 'og:image', content: this.imageVideoUrl })
            metas.push({ hid: 'og:video:url', property: 'og:video:url', content: `https://www.youtube.com/embed/${this.videoId}` })
            metas.push({ hid: 'og:video:secure_url', property: 'og:video:secure_url', content: `https://www.youtube.com/embed/${this.videoId}` })
            metas.push({ hid: 'og:video:type', property: 'og:video:type', content: `text/html` })
            metas.push({ hid: 'og:video:width', property: 'og:video:width', content: '640' })
            metas.push({ hid: 'og:video:height', property: 'og:video:height', content: '480' })
            metas.push({ hid: 'og:video:url2', property: 'og:video:url', content: `http://www.youtube.com/v/${this.videoId}?version=3&autohide=1` })
            metas.push({ hid: 'og:video:secure_url2', property: 'og:video:secure_url', content: `https://www.youtube.com/v/${this.videoId}?version=3&autohide=1` })
            metas.push({ hid: 'og:video:type2', property: 'og:video:type', content: `application/x-shockwave-flash` })
            metas.push({ hid: 'og:video:width2', property: 'og:video:width', content: '640' })
            metas.push({ hid: 'og:video:height2', property: 'og:video:height', content: '480' })
            metas.push({ hid: 'twitter:card', property: 'twitter:card', content: 'player' })
            metas.push({ hid: 'twitter:video', property: 'twitter:video', content: `https://www.youtube.com/embed/${this.videoId}` })
            metas.push({ hid: 'twitter:player', property: 'twitter:player', content: `https://www.youtube.com/embed/${this.videoId}` })
            metas.push({ hid: 'twitter:image', property: 'twitter:image', content: this.imageVideoUrl })
            metas.push({ hid: 'twitter:player:width', property: 'twitter:player:width', content: '640' })
            metas.push({ hid: 'twitter:player:height', property: 'twitter:player:height', content: '480' })
            metas.push({ hid: 'video', itemprop: 'video', content: `https://www.youtube.com/embed/${this.videoId}` })
        }

        return {
            title: this.name,
            meta: metas
        }
    },
    methods: {
        trunc: function (str, n, wordBoundary) {
            const _isLong = str.length > n
            let _str = str.replace("\n", " ")
            _str = (_isLong) ? _str.substr(0, n - 1) : _str
            _str = (_isLong && wordBoundary) ? _str.substr(0, _str.lastIndexOf(' ')) : _str

            return (_isLong) ? [_str, '...'] : [_str]
        },
        loadCurrentUser: function () {
            this.loading = true
            if (authUtils.check(this.$store.state.token)) {
                this.$axios.$get(`/promos/${this.id}/hasUser`).then(res => {
                    this.loading = false
                    if (res.success && res.data) {
                        this.subscribed = (res.data.id && res.data.id === this.$store.state.user.id)
                        if (this.subscribed && res.data.wonAt) {
                            this.winner = res.data
                        }
                    }
                    const subPromoId = this.$store.getters.subPromoId
                    if(subPromoId){
                        this.$store.commit('setSubPromoId', '')
                        if(subPromoId === this.id && !this.subscribed) {
                            this.$scrollTo('#btnSubscribe', { offset: -100 })
                            this.btnSubscribe()
                        }                        
                    }
                }).catch(err => {
                    this.loading = false
                })
            } else {
                this.loading = false
            }
        },
        btnAllDesc: function () {
            this.allDesc = true
            this.$ga.event('More Promo Desc Link', 'click', `${this.id}`)
        },
        btnAllTerms: function () {
            this.allTerms = true
            this.$ga.event('More Promo Terms Link', 'click', `${this.id}`)
        },
        btnShare: function (network, url) {
            this.$ga.event('Share Promo Button', `click: ${network}`, `${url}`)
        },
        btnSubscribe: function () {
            if (!authUtils.check(this.$store.getters.token)) {
                this.$ga.event('Subscribe Promo Button', 'click: without login', `${this.id}`)
                this.$store.commit('setSubPromoId', this.id)
                this.$router.push({ name: 'login', query: { t: this.$route.fullPath } })
            } else {
                this.$ga.event('Subscribe Promo Button', 'click: with login', `${this.id}`)
                const self = this
                this.$axios.$post(`/promos/${self.id}/user`).then(res => {
                    if (res.success) {
                        self.subscribed = true
                        this.dialogSubscribed = true
                        this.totalUsers++
                        this.loadCurrentUser()
                    }
                    else if (res.message) {
                        this.$refs.alert.showError('Ops', res.message)
                    }
                }).catch(err => {
                    console.error(err)
                    if (err.response.status === 401) {
                        this.$router.push('login?t=' + this.$route.fullPath)
                    } else {
                        this.$router.push('/ops#500')
                    }
                })
            }
        }
    }
}