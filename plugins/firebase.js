import firebase from 'firebase/app'

export default () => {

  if (!firebase.apps.length) {
    let config = {}
    if(process.env.NODE_ENV === 'production') {
      config = {
        apiKey: '',
        authDomain: '',
        databaseURL: '',
        projectId: '',
        storageBucket: '',
        messagingSenderId: ''      
      }  
    } else {
      config = {
        apiKey: '',
        authDomain: '',
        databaseURL: '',
        projectId: '',
        storageBucket: '',
        messagingSenderId: ''      
      }  
    }
    firebase.initializeApp(config) 
  }
  
}
