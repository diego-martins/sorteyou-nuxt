import Vue from 'vue'
import moment from 'moment'

export default ({ app }) => {
    Vue.filter('formatDatetime', (value, format) => {
        if (value && format) {
            return moment(String(value)).format(format)
        }
        return ''
    })
    
    Vue.filter('formatDate', (value, format) => {
        if (value && format) {
            return moment(String(value)).format(format)
        }
        return ''
    })

    Vue.filter('currency', (value, currency, fixed) => {
        let val = parseFloat(value)
        if (!isNaN(val)) {
            currency = currency || 'USD'
            fixed = parseInt(fixed)
            let amount = val.toFixed(!isNaN(fixed) ? fixed : 2)
            if(currency === 'USD') {
                amount = String(amount).replace(',', '.')
                return `$ ${amount}`
            } else if (currency === 'BRL') {
                amount = String(amount).replace('.', ',')
                return `R$ ${amount}`
            }
            return amount
        }
        return value
    })

    Vue.filter('number', (value, separator) => {
        if (value >= 1000) {
            separator = separator || '.'
            let strVal = String(value)
            let parts = []
            while(strVal.length > 3) {
                parts.push(strVal.slice(-3))
                strVal = strVal.slice(0, -3)
            }
            parts.push(strVal)
            return parts.length ? parts.reverse().join(separator) : parts[0]
        }
        return value
    })

}