export default ({ app, $axios, store, env }) => {    
    $axios.defaults.baseURL = env.apiUrl
    $axios.interceptors.request.use(config => {
        if (store.getters.token) {
            config.headers['Authorization'] = `Bearer ${store.getters.token}`
        }
        config.headers['Accept-Language'] = app.i18n.locale
        return config
    }, err => {
        return Promise.reject(err)
    })
}